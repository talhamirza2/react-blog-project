import React from 'react';

export default () => {
  return (
    <footer>
      <div className="footer">
        <ul>
          <li>React JS</li>
          <li>React Router</li>
          <li>Draft JS</li>
          <li>Love</li>
          <li>By <a href="https://www.google.pk" rel="noopener noreferrer" target="_blank">Talha Mirza</a></li>
        </ul>
      </div>
    </footer>
  )
}
